// 1. Опишіть своїми словами що таке Document Object Model (DOM).
// Document Object Model (DOM) – це об’єктна модель документа, яка показує повний зміст сторінки у вигляді об’єктів, які можна змінювати.
//
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML – отримуємо HTML розмітку елементу та його дочірніх елементів, тобто вміст елемента, а innerText – 
//отримуємо лише текстову складову елементу та його дочірніх елементів, тобто тільки текстовий вміст елемента всередині тега.
//
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Є багато способів звернутися до елемента на сторінці. Найкращими способами є:
// -	document.getElementById(id) – доступ до елемента за його id;
// -	document.querySelectorAll(css) – універсальний метод, повертає всі елементи які задовільняють css-селектор. 
// -	document.querySelector(css) – повертає перший елемент, який задовільняє css-селектор.
// Методи застарілі, але доступні до використання в окремих випадках:
// -	document.retElementsByTagName(tag) – шукає елементи за даним тегом;
// -	document.retElementsByTagClassName(className) – повертає елементи які мають даний css-клас.
// -	document.retElementsByName(name) – повертає елементи із заданим атрибутом name.
//
//
// Challenge 1

let paragraphColor = document.querySelectorAll("p");
paragraphColor.forEach((p) => p.style.backgroundColor = "#FF0000")
console.log(paragraphColor); 

// Challenge 2

let element = document.getElementById("optionsList");
console.log(element);
console.log(element.parentElement);
for (let node of element.childNodes) {
  console.log(node.nodeName, node.nodeType);
};

// Challenge 3

let paragraphChange = document.querySelector("#testParagraph");
paragraphChange.innerHTML = "This is a paragraph";
console.log(paragraphChange);

// Challege 4

let nestedElems = document.querySelectorAll(".main-header");
nestedElems.forEach((el) => el.classList.add("nav-item"));
console.log(nestedElems);

// Challenge 5

let sectionElems = document.querySelectorAll(".section-title");
sectionElems.forEach((del) => del.classList.remove("section-title"));
console.log(sectionElems);


